#!/usr/bin/php
<?php

    declare(ticks = 1);
    $_SESSION['wlIsRun'] = false;

    if(isset($argv[1]) && isset($argv[2]) && isset($argv[3])){
        if($argv[1] == '--start-testing' && strpos($argv[2],'--url=') !== false && strpos($argv[3],'--config=') !== false)
        {
            startTesting($argv[2], $argv[3]);
        }else{
            syntaxError();
        }

    }else{
        syntaxError();
    }

    /**
     * @param $url
     * @param $config
     */
    function startTesting($url, $config)
    {
        if($_SESSION['wlIsRun'] == true)
        {
            die("~\e[0;34m [wlProxy]: \e[0;37m*Error! Proxy already starts.\n");

        }elseif((parseConfig($config) === true)&&(parseUrl($url) === true)){
                $testFileContent = file_get_contents($_SESSION['testFile']);
                $swapFileContent = file_get_contents($_SESSION['swapFile']);
                if($testFileContent && $swapFileContent && !empty($testFileContent) && !empty($swapFileContent))
                {
                    $tmp = $testFileContent;
                    if(file_put_contents($_SESSION['testFile'], $swapFileContent) &&
                    file_put_contents($_SESSION['swapFile'].'.swap', $tmp))
                    {
                        $_SESSION['wlIsRun'] = true;
                        print("~\e[0;34m [wlProxy]: \e[0;39m*Done! \e[0;37mProxy starts successfull.\n");
                        print("~\e[0;34m [wlProxy]: \e[0;5m*WORKING*\e[m (to correctly stop please press CTRL+C)\n");
                        if(!file_exists($_SESSION['logFile'])){
                            file_put_contents($_SESSION['logFile']);
                        }
                        $log = file_get_contents($_SESSION['logFile']);
                        pcntl_signal(SIGINT, "signalHandler");
                        pcntl_signal(SIGTERM, 'signalHandler');
                        while(true)
                        {
                          $new_log = file_get_contents($_SESSION['logFile']);
                          if($log != $new_log){
                            print(shell_exec('tail -n 1 '. $_SESSION['logFile']));
                            $log = $new_log;
                          }
                        }
                    }
                }else{
                  Error();
                }

        }else{
            Error();
        }
    }

    function signalHandler($signo)
    {
            $swap_content = file_get_contents($_SESSION['swapFile'].'.swap');
            if(file_put_contents($_SESSION['testFile'], $swap_content)) {
                print("~\e[0;34m [wlProxy]: \e[0;35m*STOPS*\e[m\n");
                exit;
            }else{
                print("~\e[0;34m [wlProxy]: \e[0;35m*ERROR*\e[m Unnable to stops correctly. Please do not start script after see this. Before, swap content in original testing file by content in .swap file!!!!\n");
            }

    }

    /**
     * @param $new_url
     * @return bool
     */
    function parseUrl($new_url)
    {
        $parse = explode("=",$new_url);
        $global_content = json_decode(file_get_contents(__DIR__.'/global.json'),true);
        $global_content['currentUrl'] = $parse['1'];
        $global_content['logFile'] = $_SESSION['logFile'];
        if(file_put_contents(__DIR__.'/global.json',json_encode($global_content)))
            print("~\e[0;34m [wlProxy]: \e[0;39m*Done! \e[0;37mNew url for proxy was successfully set.\n");
        return true;
    }

    /**
     * @param $config_file
     * @return bool
     */
    function parseConfig($config_file)
    {
        $parse = explode("=", $config_file);
        $_SESSION['wpProxyConfigFile'] = $parse[1];
        if(file_exists($parse[1])){

            $content = json_decode(file_get_contents($parse[1]), true);
            if($content == NULL || !file_exists($content['testFile']) || !file_exists($content['swapFile']))
            {
                configEmpty();
            }else{
                $_SESSION['testFile'] = $content['testFile'];
                $_SESSION['swapFile'] = $content['swapFile'];
                $_SESSION['logFile'] = $content['logFile'];
                print("~\e[0;34m [wlProxy]: \e[0;39m*Done! \e[0;37mNew config file was successfully set.\n");
                return true;
            }

        }else{
            configError();
        }
    }

    /**
     * @error
     */
    function syntaxError()
    {
        return die("~\e[0;34m [wlProxy]: \e[0;35m*Syntax Error! \e[0;37mIncorrect arguments, please check --help for details\n");
    }

    /**
     * @error
     */
    function configError()
    {
        return die("~\e[0;34m [wlProxy]: \e[0;35m*Error!\e[m \e[0;37mUnable to find config file, check path or file name.\n");
    }

    /**
     * @error
     */
    function Error()
    {
        return die("~\e[0;34m [wlProxy]: \e[0;35m*Error!\e[m \e[0;37mPlease, fix your problems for correct work with wlProxy..\n");
    }

    /**
     * @error
     */
    function configEmpty()
    {
        return die("~\e[0;34m [wlProxy]: \e[0;35m*Error!\e[m \e[0;37mYour config file does not have JSON content!\n");

    }
?>
