<?php

namespace Proxy;

class wlProxy
{
    private $route_to;
    private $data;
    private $logFile;

    function __construct($data)
    {
        $global_content = json_decode(file_get_contents(__DIR__."/global.json"),true);
        $this->route_to = $global_content['currentUrl'];
        $this->logFile = $global_content['logFile'];
        $this->data = $data;
        echo $this->init();
    }

    public function init()
    {
            try{
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $this->route_to);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->data));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json')
                );
                $server_output = curl_exec($ch);

                curl_close($ch);

                if ($server_output) {

                    $this->wlLog('New transfered data! From:'."\r".$_SERVER['REQUEST_URI'].'. To:'.$this->route_to.'. Status: success');

                    return $this->wlSuccess([
                        'ok'
                    ]);
                }else{

                    $this->wlLog('New transfered data! From:'."\n".$_SERVER['REQUEST_URI'].'. To:'.$this->route_to.'. Status: error [NOT HAVE ANY RESPONSE FROM THE DESTINATION SERVER!]');

                    return $this->wlError([
                        'curl error'
                    ]);
                }
            }catch(\RuntimeException $e){
                echo "cURL error ". $e->getMessage(). "!";
            }
    }

    public function wlLog(string $log_data)
    {
        $string = "~ \e[0;32m[wlLog]\e[m: [".date('Y-m-d H:i:s')."] \e[0;37m".$log_data.PHP_EOL;

        file_put_contents($this->logFile, $string, FILE_APPEND | LOCK_EX);
    }


    public function wlSuccess(array $result)
    {
        return json_encode($success = [
            'status' => 'ok',
            'result' => $result
        ]);
    }

    public function wlError(array $result)
    {
        return json_encode($success = [
            'status' => 'error',
            'result' => $result
        ]);
    }
}
?>
