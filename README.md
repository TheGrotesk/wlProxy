**Configure wlProxy testing**

Go to directory with wlProxy
Create json config file and past in this

```json
    {
      "testFile": "path to testing file",
      "swapFile": "path to swap file",
      "logFile" : "path to log file"
    }
```

Where
- [ ] "testFile" is file which we will be testing,
- [ ] "swapFile" is file which swap original testing file with himself
- [ ] "logFile" is file  which receive logs after proxy done his job.

Next step is write swap file for testing which we wrote in config file
- [ ] Create file "swapFiles/AndroidSubscription.wlproxy.php"
- [ ] And paste new data which replace original data in original file

```php

    public function notify(Request $request, Response $response)
    {
            $data = $request->getParsedBody();

            $this->subscriptionsLogger->info('New Android notification: ', $data);

            $proxy = new wlProxy($data);
    }

```

Next step is run the wlProxy via terminal with arguments
```
- php wlProxyRun.php --start-testing --url= [url which receive data from this server]
 --config=[name of config file which we made]
```

Example
```
- php wlProxyRun.php --start-testing --url=https://53423ngrok.io/api/v1/google/notify_test --config=config.android.json
```

Stop the job
```
- To stop proxy just press Ctrl+C
```
